# Frequency detector

Detects the presence of 100Hz - 300Hz sine wave using DSP functions.
ADC is sampled at A0 pin @ 10kHz, directly to DMA buffer. When full, passed to DSP functions.

## Deployment

Generate a project for STM32F401RE using STM32CubeMX for KEIL5, replace with provided files, compile and run.

## License

This project is licensed under the MIT License.